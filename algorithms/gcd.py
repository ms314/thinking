#!/usr/bin/env python3


def gcd(m: int, n: int) -> int:
    r = m % n
    if r == 0:
        return n
    return gcd(n, r)


if __name__ == "__main__":
    res = gcd(5, 2)
    print(res)
