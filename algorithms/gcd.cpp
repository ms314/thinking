#include <iostream>

using namespace std;

int gcd_rec(int m, int n, int c) {
  if (n == 0) {
    printf("counter == %d\n", c - 1);
    return m;
  }
  return gcd_rec(n, m % n, c + 1);
}

int gcd(int m, int n) {
  int c = 0;
  int r = 1;
  while (r != 0) {
    r = m % n;
    m = n;
    n = r;
    c += 1;
  }
  return c;
}

int main() {
  for (int m = 1; m < 2100; m++) {
    int r = gcd(m, 5);
    printf("For m=%2d c=%2d \n", m, r);
  }
}
