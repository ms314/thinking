#include <bits/stdc++.h>
#include <cstdio>
#include <iostream>
#include <vector>

/*

Idea:

We need to check how many coins is needed for x - c for c in coins.
Choose optimal and add 1.

In order to find optimal for (x-c) we can use recursive call, or we can store previously calculated results into array.
*/
using namespace std;

typedef vector<int> vi;
typedef pair<int, int> pi;

const int INF = std::numeric_limits<int>::max() - 1;

class SolutionRecursive{
public:
    static int solve(int x) {
        int best = INF;
        const int coins[] = {1, 3, 4};
        if (x < 0)
            return INF;
        if (x == 0)
            return 0;
        for (int c: coins)
            best = min(solve(x - c) + 1, best);
        return best;
    }

};


class SolveWithMemoization {
public:
    bool ready[1000] = {false};
    int value[1000] = {-1};
    int solve(int x) {
        int best = INF;
        const int coins[] = {1, 3, 4};
        if (x < 0)
            return INF;
        if (x == 0)
            return 0;
        if (ready[x])
            return value[x];
        for (auto c: coins)
            best = min(solve(x - c) + 1, best);
        ready[x] = true;
        value[x] = best;
        return best;
    }
};

class SolutionIterative{

public:
    bool ready[1000] = {false};
    int value[1000] = {-1};
    int solve_for(int x) {
        const int coins[] = {1, 3, 4};
        int best = INF;
        for(int c: coins){

        }
        return best;
    }
};

int main() {
    auto s_mem = new SolveWithMemoization();
    auto s2 = new SolutionIterative();
    s2->solve_for(5);
    for (int sum: {1, 2, 3, 4, 5, 6, 7})
        cout << sum << " -> "
             << SolutionRecursive::solve(sum) << " == "
             << s_mem -> solve(sum) << endl;
}
