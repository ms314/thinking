#!/usr/bin/env python3
N = 3
subsets = []


def search(k):
    if k < N + 1:
        subsets.append(k)
        search(k + 1)
        subsets.pop()
        search(k + 1)
    else:
        print(subsets)


if __name__ == "__main__":
    search(1)
