#include <bitset>
#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

int N = 3;
vector<int> subset;


// Draw recursion call tree.
void search(int k) {
  // what is k?
  if (k < N + 1) {
    subset.push_back(k);
    search(k + 1); // makes two others recursive calls
    subset.pop_back();
    search(k + 1); // makes two others recursive calls
  } else {
    for (int el: subset)
      cout << el << ' ';
    cout << endl;
  }
}

int main() {
  search(1);
}
