#include <stdio.h>

int gcd(int m, int n) {
  int r = 1;
  while (r != 0) {
    r = m % n;
    if (r == 0)
      break;
    m = n;
    n = r;
  }
  return n;
}

int main() {
  for (int m = 289; m < 290; m++) {
    int res = gcd(55, 33);
    printf("GCD = %2d\n", res);
  }
}
