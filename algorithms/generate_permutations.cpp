#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

int N = 3;
vector<int> permutation;
bool pushed[4];

/*
** Generate trees.
**
** 1 - 12 - 123
**   - 13 - 132
**
** 2 - 21 - 213
**   - 23 - 231
**
** 3 - 31 - 312
**   - 32 - 321
*/

void search() {
  if (permutation.size() == N) {
    for (int el : permutation)
      cout << el << ' ';
    cout << endl;
  } else {
    for (int i = 1; i < N + 1; i++) {
      // if (pushed[i])
      // continue;
      permutation.push_back(i);
      search();
      permutation.pop_back();
      // pushed[i] = false;
      search();
    }
  }
}

int main() { search(); }
