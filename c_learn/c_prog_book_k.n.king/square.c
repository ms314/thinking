#include <stdio.h>

int main(void){
    printf("This program prints a table of squares.\n");
    printf("Enther number of entries in table:\n");
    int i, n;
    scanf("%d", &n);
    for (i=1; i<=n; i++){
        printf("%10d%10d\n", i, i*i);
    }
    return 0;
}
