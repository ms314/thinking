#include <stdio.h>
#include <string.h>


typedef struct {
    char *name;
    int age;
    char postcode[8];
} Person;

int main() {
    Person person;
    person.age = 20;
    person.name = "John Doe";
    person.postcode[0] = '1';
    strcpy(person.postcode, "12345678");
    printf("Name: %s\n", person.name);
    printf("Age: %d\n", person.age);
    printf("Postcode: %s\n", person.postcode);
    return 0;
}