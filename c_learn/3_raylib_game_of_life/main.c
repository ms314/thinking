#include "raylib.h"
#include "stdio.h"

//----------------------------------------------------------------------------------
// Some Defines
//----------------------------------------------------------------------------------
#define SQUARE_SIZE 10
// #define OFFSET 10
#define NUM_OF_SQUARES 50

//----------------------------------------------------------------------------------
// Types and Structures Definition
//----------------------------------------------------------------------------------
typedef enum GridSquare { L = 1, D = 0 } GridSquare;

typedef struct Grid {
  int width;
  int height;
} Grid;

//------------------------------------------------------------------------------------
// Global Variables Declaration
//------------------------------------------------------------------------------------
static const int screenWidth = 500;
static const int screenHeight = 500;

static bool simulationOver = false;
static bool pause = false;
static bool gameOver = false;

// Matrices
static GridSquare grid[NUM_OF_SQUARES][NUM_OF_SQUARES];
// static GridSquare piece[4][4];

//------------------------------------------------------------------------------------
// Module Functions Declaration (local)
//------------------------------------------------------------------------------------
static void InitGame(void);       // Initialize game
static void UpdateGameData(void); // Update game (one frame)
static void DrawGame(void);       // Draw game (one frame)

int main(void) {
  InitWindow(screenWidth, screenHeight, "Window");
  InitGame();

  SetTargetFPS(20);
  while (!WindowShouldClose()) {

    Vector2 mouse = GetMousePosition();
    if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT))
      printf("%f %f\n", mouse.x, mouse.y);
    int i_grid = ((int)mouse.x / NUM_OF_SQUARES) * SQUARE_SIZE;
    int j_grid = ((int)mouse.y / NUM_OF_SQUARES) * SQUARE_SIZE;

    UpdateGameData();
    DrawGame();
  }
  CloseWindow();
  return 0;
}

#define INIT_RANDOM false

void InitGame(void) {
  int random;
  pause = false;
  if (INIT_RANDOM) {
    for (int i = 0; i < NUM_OF_SQUARES; i++) {
      for (int j = 0; j < NUM_OF_SQUARES; j++) {
        random = GetRandomValue(0, 5);
        if (random == 1)
          grid[i][j] = L;
        else
          grid[i][j] = D;
      }
    }
    for (int i = 0; i < NUM_OF_SQUARES; i++) {
      for (int j = 0; j < NUM_OF_SQUARES; j++) {
        printf("%d ", grid[i][j]);
      }
      printf("\n");
    }
  } else {
  }
}

void UpdateGameData(void) {
  int random;

  if (!gameOver) {
    if (IsKeyPressed('P'))
      pause = !pause;
    if (!pause) {

      for (int i = 1; i < NUM_OF_SQUARES - 1; i++) {
        for (int j = 1; j < NUM_OF_SQUARES - 1; j++) {
          int count = grid[i - 1][j] + grid[i - 1][j - 1] + grid[i][j - 1] +
                      grid[i + 1][j - 1] + grid[i + 1][j] + grid[i + 1][j + 1] +
                      grid[i][j + 1] + grid[i - 1][j + 1];
          if (grid[i][j] == L) {
            if (count < 2 || count > 3)
              grid[i][j] = D;
            if (count == 2 || count == 3)
              grid[i][j] = L;
          } else if (grid[i][j] == D) {
            if (count == 2)
              grid[i][j] = L;
          }
        }
      }
    }
  }
}

void DrawGame(void) {
  BeginDrawing();

  for (int i = 0; i < NUM_OF_SQUARES; i++) {
    for (int j = 0; j < NUM_OF_SQUARES; j++) {
      if (grid[i][j] == L) {
        DrawRectangle(i * SQUARE_SIZE, j * SQUARE_SIZE, SQUARE_SIZE,
                      SQUARE_SIZE, WHITE);
      } else if (grid[i][j] == D) {
        DrawRectangle(i * SQUARE_SIZE, j * SQUARE_SIZE, SQUARE_SIZE,
                      SQUARE_SIZE, BLACK);
      }
    }
  }

  EndDrawing();
}
