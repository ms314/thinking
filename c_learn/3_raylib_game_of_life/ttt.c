#include <stdio.h>

typedef struct G {
  int width;
  int height;
} Grid;

int main(void) {

  Grid g;
  g.width = 10;
  g.height = 11;
  printf("%d\n", g.height);
  printf("%d\n", g.width);
  return 0;
}
