#include "stdio.h"
#include <stdlib.h>
// \n
int main(void) {
  const char *input = "FE1234re";
  int N = sizeof(input);
  for (int i = 0; i < N; i++) {
    printf("%d ", input[i]);
  }
  printf("\n");
  for (int i = 0; i < N; i++) {
    printf("atoi == %d ", atoi(input[i]));
  }
  printf("\n");
  for (int i = 0; i < N; i++) {
    printf("%c ", input[i]);
  }
  printf("\n");

  return 0;
}
