#include <iostream>

#include "TokenType.h"

class Token{
public:
    Token(TokenType type, std::string& lexeme, void* literal, int line): 
    type(type), lexeme(lexeme), literal(literal), line(line) {};
    const TokenType type;
    const std::string lexeme;
    void* literal;
    const int line;
};