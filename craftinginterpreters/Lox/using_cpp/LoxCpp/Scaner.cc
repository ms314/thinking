#include <unordered_map>
#include <iostream>
#include <vector>

#include "TokenType.h"
#include "Token.h"

class Scanner{
public:
    Scanner(const std::string& input): source(input){};

    std::vector<Token> scanTokens() {
        while (!isAtEnd())
        {
            start = current;
            scanToken();
        }
        tokens.push_back(Token(TokenType::EOFL, "", nullptr, line));
        return tokens; 

    };

    void display() {
        std::cout << source << std::endl;
    };

private:
    std::string source;
    std::vector<Token> tokens;
    int start {0};
    int current {0};
    int line {0};
    static std::unordered_map<std::string, TokenType> keywords;

    bool isAlpha(char c) {
        return (c >= 'a' && c <= 'z') ||
               (c >= 'A' && c <= 'Z') ||
               (c == '_');
    };

    bool isAlphaNumeric(char c) {
        return isAlpha(c) || isDigit(c);
    };

    bool isDigit(char c) {
        return c >= '0' && c <= '9';
    };

    bool isAtEnd(){
        return current > source.length();
    };

    char advance() {
        return source.at(current++);
    }

    char peek() {
        if(isAtEnd()) return '\0';
        return source.at(current);
    }

    char peekNext() {
        if(current + 1 >= source.length()) return '\0';
        return source.at(current + 1);
    }

    bool match(char expected) {
        if (isAtEnd()) return false;
        if (source.at(current) != expected) return false;
        current++;
        return true;
    };


};


std::unordered_map<std::string, TokenType> Scanner::keywords = {
    {"and",    TokenType::AND},
    {"class",  TokenType::CLASS},
    {"else",   TokenType::ELSE},
    {"false",  TokenType::FALSE},
    {"for",    TokenType::FOR},
    {"fun",    TokenType::FUN},
    {"if",     TokenType::IF},
    {"nil",    TokenType::NIL},
    {"or",     TokenType::OR},
    {"print",  TokenType::PRINT},
    {"return", TokenType::RETURN},
    {"super",  TokenType::SUPER},
    {"this",   TokenType::THIS},
    {"true",   TokenType::TRUE},
    {"var",    TokenType::VAR},
    {"while",  TokenType::WHILE}
};