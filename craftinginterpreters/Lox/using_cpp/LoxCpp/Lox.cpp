#include <iostream>
#include <memory>
#include <vector>

// MAC #include <editline/history.h>
#include <editline/readline.h>

#include "Scaner.h"

int main() {
    std::cout << "Lox Version 0.0.1";
    std::cout << "Press Ctrl+c to Exit\n";
    Scanner s("define func");
    s.display();

    while (1) {
        char *input = readline("lox ");
        add_history(input);
        std::cout << input;
        free(input);
    }

    return 0;
}