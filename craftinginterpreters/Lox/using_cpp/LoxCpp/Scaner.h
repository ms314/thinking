#ifndef SCANNER_H
#define SCANNER_H

#include <unordered_map>
#include <iostream>
#include <vector>

#include "TokenType.h"
#include "Token.h"

class Scanner{
public:
    Scanner(const std::string& input){};
    void display() {};

private:
    static std::unordered_map<std::string, TokenType> keywords;
    std::vector<Token> tokens;
    std::string input;
    int start;
    int current;
    int line;
};


#endif // SCANNER_H