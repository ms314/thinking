
Versions history
=================


**git tag v0.0.5-craftinginterpreters**  
More advanced statements with Environment,

**git tag v0.0.4-craftinginterpreters**  
Aded statements (e.g. `print 1+2;`).

**git tag v0.0.3-craftinginterpreters**  
Added exaluation.

**git tag v0.0.2-craftinginterpreters**  
Parsing works.

**git tag v0.0.1-craftinginterpreters**  
Scanner works.
