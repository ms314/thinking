import java.text.DecimalFormat;

interface Visitor {
    public double visit(Liquor liquorItem);
    public double visit(Tobacco tobaccoItem);
}

interface Visitable {
    public double accept(Visitor visitor);
}

class TaxVisitor implements Visitor {
    DecimalFormat df = new DecimalFormat("#.##");
    public TaxVisitor() { }
    public double visit(Liquor liquorItem) {
        double res = Double.parseDouble(df.format((liquorItem.getPrice() * 0.1) + liquorItem.getPrice()));
        System.out.println("Liquor Item: Price with Tax = " + res);
        return res;
    }
    public double visit(Tobacco tobaccoItem) {
        double res = Double.parseDouble(df.format((tobaccoItem.getPrice() * 0.3) + tobaccoItem.getPrice()));
        System.out.println("Tobacco Item: Price with Tax = " + res);
        return res;
    }
}

class Liquor implements Visitable {
    private double price;
    Liquor(double item) {price = item;}
    public double accept(Visitor visitor) {
        return visitor.visit(this);
    }
    public double getPrice() {
        return price;
    }
}

class Tobacco implements Visitable {
    private double price;
    Tobacco(double item) {price = item;}
    public double accept(Visitor visitor) {
        return visitor.visit(this);
    }
    public double getPrice() {
        return price;
    }
}

class VisitorExample {
    public static void main(String[] args) {
        TaxVisitor tv = new TaxVisitor();
        Liquor l_obj = new Liquor(10);
        Tobacco t_obj = new Tobacco(20);
        tv.visit(l_obj);
        tv.visit(t_obj);
    }
}