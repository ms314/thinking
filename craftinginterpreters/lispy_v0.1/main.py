#!/usr/bin/env python3
from reader import parse
from eval import eval


if __name__ == "__main__":
    r = eval(parse("(begin (define r 10) (* pi (* r r)))"))
    prg = "(define circle-area (lambda (r) (* pi (* r r))))"
    prg += "(circle-area 3)"
    print(eval(parse(prg)))
