#!/usr/bin/env python3

from types_m import *


def parse(program: str) -> Exp:
    return _read_from_tokens(_tokenize(program))


def _tokenize(chars: str) -> list:
    return chars.replace("(", " ( ").replace(")", " ) ").split()


def _read_from_tokens(tokens: list) -> Exp:
    if len(tokens) == 0:
        raise SyntaxError("unexpected EOF")
    # advance
    token = tokens.pop(0)
    if token == "(":
        L = []
        while tokens[0] != ")":
            L.append(_read_from_tokens(tokens))
        tokens.pop(0)  # pop off ')'
        return L
    elif token == ")":
        raise SyntaxError("unexpected )")
    else:
        return _atom(token)


def _atom(token: str) -> Atom:
    try:
        return int(token)
    except ValueError:
        try:
            return float(token)
        except ValueError:
            return Symbol(token)
