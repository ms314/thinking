#include <stdio.h>

/* p20 Ex 1-10 */
int main(void) {
  int c;
  while ((c = getchar()) != EOF) {
    if (c == ' ')
      putchar('.');
    else if (c == '\t') {
      putchar('\\');
      putchar('t');
    } else if (c == '\\') {
      putchar('\\');
    } else
      putchar(c);
  }
}
