#include <stdio.h>

/* p.20, 1.9 replace multiple blanks by 1 blank */
int main(void) {
  int c, nb;
  nb = 0;
  while ((c = getchar()) != EOF) {
    if (c == ' ')
      ++nb;
    else
      nb = 0;
    if (nb > 1)
      continue;
    putchar(c);
  }
  return 0;
}
