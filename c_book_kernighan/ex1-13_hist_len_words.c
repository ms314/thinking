#include <stdio.h>

#define MAXLEN 100

/* p24 print histogram of lengths of words */
int main(void) {
  int i, j, v, c, counter;
  int w_lengths[MAXLEN];

  for (i = 0; i < MAXLEN; ++i)
    w_lengths[i] = 0;
  counter = 0;

  while ((c = getchar()) != EOF) {
    if (c == ' ' || c == '\t' || c == '\n') {
      if (counter > 0) {
        ++w_lengths[counter];
        counter = 0;
      }
    } else {
      ++counter;
    }
  }

  for (i = 0; i < MAXLEN; ++i) {
    if ((v = w_lengths[i]) > 0) {
      printf("%2d ", i);
      for (j = 0; j < v; ++j) {
        printf("%s", "#");
      }
      printf("\n");
    }
  }
}
