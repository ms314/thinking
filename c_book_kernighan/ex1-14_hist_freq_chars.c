#include <stdio.h>

#define NUM_OF_CHARS 1000
/* ex 1-14 print a histogram of the ferequencies of different characters   */
int main(void) {
  int i, j, c, c_num_code, v, max_count, v_scaled;
  int characters_counts[NUM_OF_CHARS];
  int characters[NUM_OF_CHARS];

  max_count = 0;

  // init
  for (int i = 0; i < NUM_OF_CHARS; ++i) {
    characters_counts[i] = 0;
    characters[i] = '\0';
  }

  // collect
  while ((c = getchar()) != EOF) {
    c_num_code = c - '0';
    ++characters_counts[c_num_code];
    characters[c_num_code] = c;
  }

  // find max count
  for (i = 0; i < NUM_OF_CHARS; ++i) {
    if (max_count < characters_counts[i])
      max_count = characters_counts[i];
  }

  // plot hist
  for (i = 0; i < NUM_OF_CHARS; ++i) {
    if ((v = characters_counts[i]) > 0) {
      printf("%c (%3d): ", characters[i], v);
      v_scaled = (int)(v * 10 / max_count);
      for (j = 0; j < v_scaled; ++j) {
        printf("%s", "#");
      }
      printf("\n");
    }
  }

  printf("max count = %d\n", max_count);
  printf("%d\n", 1 / 2);

  return 0;
}
