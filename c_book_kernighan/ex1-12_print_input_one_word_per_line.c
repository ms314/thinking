#include <stdio.h>

/* p21  */
int main(void) {
  int c, counter;
  counter = 0;
  while ((c = getchar()) != EOF) {
    if (c == ' ' || c == '\t' || c == '\n')
      ++counter;
    else
      counter = 0;

    if (counter == 1)
      putchar('\n');
    if (counter == 0)
      putchar(c);
  }
  return 0;
}
