#include <stdio.h>

/*p.20 count blanks, tabs, newlines*/
int main(void) {
  int c, nb, nt, nl;
  nb = nt = nl = 0;
  while ((c = getchar()) != EOF) {
    if (c == '\n')
      ++nl;
    else if (c == '\t')
      ++nt;
    else if (c == ' ')
      ++nb;
  }
  printf("%d blanks, %d tabs, %d lines ", nb, nt, nl);
}
