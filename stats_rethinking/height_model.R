library(rethinking)

data(Howell1)

d <- Howell1

str(d)

precis(d)

d2 <- d[ d$age >= 18, ]

# Prior predictive simulation
#
sample_mu <- rnorm( 1e4, 178, 20 )
sample_sigma <- runif( 1e4, 0, 50 )
prior_h <- rnorm( 1e4, sample_mu, sample_sigma )
dens( prior_h )

#
plot(d2$height - d2$weight)
